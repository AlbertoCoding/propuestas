## Entrenamientos de hacking con recursos open source

Igual que los deportistas de élite necesitan entrenar duramente para mejorar su rendimiento y resultados en las competiciones,
los profesionales de la ciberseguridad y los hackers necesitan hacer lo mismo para desempeñar su trabajo y estar al día de la evolución tecnológica en la que vivimos.


## Formato de la propuesta

Indicar uno de estos:

* [X] Charla (25 minutos)
* [ ] Charla relámpago (10 minutos)


## Descripción

En esta charla hablaremos sobre qué es el pentesting, sus fases, áreas que involucra, herramientas importantes, programas más utilizados y sobre todo,
nos centraremos en conocer diferentes servicios abiertos disponibles para entrenar y aprender más sobre las técnicas actuales y el hacking.
Si quieres comenzar en el mundo del hacking, mejorar tus conocimientos o conocer cómo atacaría un hacker, es el momento de descubrir qué opciones tenemos 
disponibles para aprender y entrenar nuestras habilidades. Igual que los deportistas de élite necesitan entrenar duramente para mejorar su rendimiento y
resultados en las competiciones, los profesionales de la ciberseguridad y los hackers necesitan hacer lo mismo para desempeñar su trabajo y estar al día 
de la evolución tecnológica en la que vivimos.

Haremos un repaso de CTFs, laboratorios e infraestructuras de entrenamiento, desafíos y sitios vulnerables que nos servirán para estar preparados
ante una situación real o mejorar nuestras habilidades. También cheatsheets, write-ups y repositorios github que nos ayuden a acelerar nuestro aprendizaje.

Algunas de estas infraestructuras de entrenamiento serán:

OWASP-SKF Labs (OWASP) - Ref.[https://github.com/blabla1337/skf-labs]
Web For Pentester (WFP) - Ref.[https://pentesterlab.com/exercises/web_for_pentester/course]
Damn Vulnerable Web Application (DVWA) - Ref.[http://www.dvwa.co.uk/]
The Juice Shop (OWASP) - Ref.[https://owasp.org/www-project-juice-shop/]
VulnHub - Ref.[https://www.vulnhub.com/]


## Público objetivo

Curiosos, estudiantes, profesionales o cualquier persona interesada en aprender sobre ciberseguridad o concretamente sobre hacking y pentesting.


## Ponente(s)

**Alberto Rafael Rodríguez Iglesias**: actualmente soy estudiante del grado de ingeniería de telecomunicaciones de la Universidad Rey Juan Carlos. 
Además, me dedico profesionalmente a la ciberseguridad desde hace un par de años.
Respecto al mundo del software libre, he participado y finalizado en 2018 y 2019 el programa Google Summer of Code (GSOC) organizado por Google, 
así como el Red Hat Open Source Contest, desarrollando código para el respositorio oficial de proyectos de ciberseguridad y hacking como
 Metasploit y OWASP.


### Contacto(s)

* Nombre: **Alberto Rafael Rodríguez Iglesias**
* Correo: albertocysec at gmail dot com


## Comentarios

Traer muchas ganas de aprender y de descubrir el potencial del software libre.


## Condiciones

* [X] Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [X] Al menos una persona entre los que la proponen estará presente el día programado para la charla.

