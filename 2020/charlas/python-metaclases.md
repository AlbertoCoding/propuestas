---
layout: 2020/post
section: propuestas
category: talks
title: Metaclases en Python&#58 el otro uso de type
---

Pequeña introducción a las metaclases en Python y a la autogeneración de código. Me parece un tema interesante y que no toda la gente conoce.

## Formato de la propuesta

-   [x] &nbsp;Charla (25 minutos)
-   [x] &nbsp;Charla relámpago (10 minutos)

## Descripción

En uno de mis últimos proyectos me encontré con un problema que solucioné utilizando las metaclases en Python, ya que podemos utilizarla para heredar de clases y generar de una forma sencilla y automática esta herencia.

Daré ejemplos reales de cómo se han utilizado las metaclases en un proyecto, para no quedarnos solo en la teoría.

## Público objetivo

Dirigida a cualquiera que sepa algo de Python y no haya utilizado a sabiendas las metaclases de Python.

## Ponente(s)

**Víctor Ramírez de la Corte**, soy freelance, trabajo actualmente con Python y Django. Soy un amante del software libre y de vim. Anteriormente he dado varias charlas en la Universidad de Sevilla y varias en las Betabeers de Huelva.

## Contacto

-   **Víctor Ramírez**: admin at virako dot es

## Comentarios

En principio el formato de 10 minutos creo que encaja, aunque dependiendo de la acogida que tenga la charla, puedo extenderla algo más y pasar al formato de 25 minutos.

## Condiciones

-   [x] &nbsp;Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x] &nbsp;Al menos una persona entre los que la proponen estará presente el día programado para la charla.
